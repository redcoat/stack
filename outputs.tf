output "api" {
  value = local.cluster_host
}

output "ingress_hostname" {
  value = local.ingress_load_balancer
}
