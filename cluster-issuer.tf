
# We need to deploy the ClusterIsser via helm rather than just using
# straight Kubernetes, as it uses a Custom Resource Definition. These
# aren't supported by the hashicorp/kubernetes provider.
resource "helm_release" "cluster-issuer" {
  name      = "cluster-issuer"
  chart     = "cluster-issuer"
  namespace = helm_release.cert_manager.namespace

  set {
    name  = "email"
    value = var.lets_encrypt_email
  }
}
